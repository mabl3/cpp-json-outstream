/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * C++ JSON Stream  https://gitlab.com/mabl3/cpp-json-outstream
 * ------------------------------------------------------------
 *
 * MIT License
 *
 * Copyright (c) 2020 Matthis Ebel
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

#ifndef DISAMBIGUATESTLCONTAINER_H
#define DISAMBIGUATESTLCONTAINER_H

namespace mabl3 {

// default: whatever you pass is not a STL container
inline constexpr auto isContainerImpl(...) {
    return std::false_type{};
}

// if C has .begin() and .end(), say it is a STL container
template <typename C>
constexpr auto isContainerImpl(C const & c)
-> decltype(begin(c), end(c), std::true_type{}) {
    return std::true_type{};
}

// if C has container_type member, say it is a STL container (true for container adaptors)
template <typename C, typename = typename C::container_type>
constexpr auto isContainerImpl(C const & c) {
    return std::true_type{};
}

// CALL THIS to find out if c is a container
template <typename C>
constexpr auto isContainer(C const & c) {
    return isContainerImpl(c);
}

// default: whatever you pass is not a STL (unordered) map
inline constexpr auto isMapContainerImpl(...) {
    return std::false_type{};
}

// if C has a key_type and value_type::first_type member, say its a STL (unordered) map
template <typename C, typename = typename C::key_type, typename = typename C::value_type::first_type>
constexpr auto isMapContainerImpl(C const &) {
    return std::true_type{};
}

// CALL THIS to find out if c is an (unordered) map
template <typename C>
constexpr auto isMapContainer(C const & c) {
    return isMapContainerImpl(c);
}

// default: whatever you pass is not a STL assiociative container
inline constexpr auto isAssociativeContainerImpl(...) {
    return std::false_type{};
}

// if C has a key_type member, say it is a STL associative container (like map, set)
template <typename C, typename = typename C::key_type>
constexpr auto isAssociativeContainerImpl(C const &) {
    return std::true_type{};
}

// CALL THIS to find out if c is an associative container (like map, set)
template <typename C>
constexpr auto isAssociativeContainer(C const & c) {
    return isAssociativeContainerImpl(c);
}

} // NAMESPACE mabl3

#endif // DISAMBIGUATESTLCONTAINER_H
