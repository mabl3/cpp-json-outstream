#include <array>
#include <deque>
#include <forward_list>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "catch2/catch.hpp"
#include "mabl3/disambiguateSTLContainer.h"

TEST_CASE("Disambiguate STL Container") {
    // just container
    auto array = std::array<int, 2>();
    auto vector = std::vector<int>();
    auto deque = std::deque<int>();
    auto fwdList = std::forward_list<int>();
    auto list = std::list<int>();
    auto stack = std::stack<int>();
    auto queue = std::queue<int>();
    auto priorityQueue = std::priority_queue<int>();

    // container + associative container
    auto set = std::set<int>();
    auto unorderedSet = std::unordered_set<int>();
    auto multiset = std::multiset<int>();
    auto unorderedMultiset = std::unordered_multiset<int>();

    // container + associative container + map
    auto map = std::map<int, int>();
    auto unorderedMap = std::unordered_map<int, int>();
    auto multimap = std::multimap<int, int>();
    auto unorderedMultimap = std::unordered_multimap<int, int>();

    SECTION("Is Container") {
        REQUIRE(mabl3::isContainer(array));
        REQUIRE(mabl3::isContainer(vector));
        REQUIRE(mabl3::isContainer(deque));
        REQUIRE(mabl3::isContainer(fwdList));
        REQUIRE(mabl3::isContainer(list));
        REQUIRE(mabl3::isContainer(stack));
        REQUIRE(mabl3::isContainer(queue));
        REQUIRE(mabl3::isContainer(priorityQueue));

        REQUIRE(mabl3::isContainer(set));
        REQUIRE(mabl3::isContainer(unorderedSet));
        REQUIRE(mabl3::isContainer(multiset));
        REQUIRE(mabl3::isContainer(unorderedMultiset));

        REQUIRE(mabl3::isContainer(map));
        REQUIRE(mabl3::isContainer(unorderedMap));
        REQUIRE(mabl3::isContainer(multimap));
        REQUIRE(mabl3::isContainer(unorderedMultimap));
    }

    SECTION("Is Associative") {        
        REQUIRE(mabl3::isAssociativeContainer(set));
        REQUIRE(mabl3::isAssociativeContainer(unorderedSet));
        REQUIRE(mabl3::isAssociativeContainer(multiset));
        REQUIRE(mabl3::isAssociativeContainer(unorderedMultiset));

        REQUIRE(mabl3::isAssociativeContainer(map));
        REQUIRE(mabl3::isAssociativeContainer(unorderedMap));
        REQUIRE(mabl3::isAssociativeContainer(multimap));
        REQUIRE(mabl3::isAssociativeContainer(unorderedMultimap));
    }
    SECTION("Not Is Associative") {
        REQUIRE(!(mabl3::isAssociativeContainer(array)));
        REQUIRE(!(mabl3::isAssociativeContainer(vector)));
        REQUIRE(!(mabl3::isAssociativeContainer(deque)));
        REQUIRE(!(mabl3::isAssociativeContainer(fwdList)));
        REQUIRE(!(mabl3::isAssociativeContainer(list)));
        REQUIRE(!(mabl3::isAssociativeContainer(stack)));
        REQUIRE(!(mabl3::isAssociativeContainer(queue)));
        REQUIRE(!(mabl3::isAssociativeContainer(priorityQueue)));
    }

    SECTION("Is Map") {        
        REQUIRE(mabl3::isMapContainer(map));
        REQUIRE(mabl3::isMapContainer(unorderedMap));
        REQUIRE(mabl3::isMapContainer(multimap));
        REQUIRE(mabl3::isMapContainer(unorderedMultimap));
    }
    SECTION("Not Is Map") {
        REQUIRE(!(mabl3::isMapContainer(array)));
        REQUIRE(!(mabl3::isMapContainer(vector)));
        REQUIRE(!(mabl3::isMapContainer(deque)));
        REQUIRE(!(mabl3::isMapContainer(fwdList)));
        REQUIRE(!(mabl3::isMapContainer(list)));
        REQUIRE(!(mabl3::isMapContainer(stack)));
        REQUIRE(!(mabl3::isMapContainer(queue)));
        REQUIRE(!(mabl3::isMapContainer(priorityQueue)));

        REQUIRE(!(mabl3::isMapContainer(set)));
        REQUIRE(!(mabl3::isMapContainer(unorderedSet)));
        REQUIRE(!(mabl3::isMapContainer(multiset)));
        REQUIRE(!(mabl3::isMapContainer(unorderedMultiset)));
    }
}
