#include <array>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "catch2/catch.hpp"
#include "mabl3/JsonStream.h"
#include "nlohmann/json.hpp"

using namespace mabl3;

TEST_CASE("JsonStream") {

    SECTION("Test JsonValue") {
        // string
        auto str = std::string("\"tr\"y this\"");
        auto val = JsonValue(str);
        REQUIRE(val.value() == "\"tr\\\"y this\"");

        // valid string
        str = "\"try this \"correctly formatted\\\" string that really will break something on purpose";
        val = JsonValue(str, JsonValue::stringIsValidJson);
        REQUIRE(val.value() == str);

        // more complicated string
        str = "[[\"hg38\",\"hg38|chr18|52340197|1195707|+|80373285|1195707|ENSG00000187323|ENST00000442544\",\"0\",\"0\"],[\"mm10\",\"mm10|chr18|71253634|1097436|-|90702639|1097436|ENSMUSG00000060534\",\"-8\",\"0\"]]";
        val = JsonValue(str);
        REQUIRE(val.value() == "\"[[\\\"hg38\\\",\\\"hg38|chr18|52340197|1195707|+|80373285|1195707|ENSG00000187323|ENST00000442544\\\",\\\"0\\\",\\\"0\\\"],[\\\"mm10\\\",\\\"mm10|chr18|71253634|1097436|-|90702639|1097436|ENSMUSG00000060534\\\",\\\"-8\\\",\\\"0\\\"]]\"");

        // bool
        REQUIRE(JsonValue(false).value() == "false");
        REQUIRE(JsonValue(true).value() == "true");

        // other values
        auto doubleNum = JsonValue(1.);
        REQUIRE(doubleNum.value() == std::to_string(1.));
        auto intNum = JsonValue(1);
        REQUIRE(intNum.value() == "1");
        auto doubleNA = JsonValue(std::numeric_limits<float>::infinity());
        REQUIRE(doubleNA.value() == "null");
        doubleNA = JsonValue(-std::numeric_limits<float>::infinity());
        REQUIRE(doubleNA.value() == "null");
        doubleNA = JsonValue(std::nanf(""));
        REQUIRE(doubleNA.value() == "null");

        std::vector<int> innerVector{1,2,3,4,5};
        std::vector<std::vector<int>> outerVector;
        outerVector.emplace_back(innerVector);
        outerVector.emplace_back(innerVector);
        auto jvector = JsonValue(outerVector);
        REQUIRE(jvector.value() == "[[1,2,3,4,5],[1,2,3,4,5]]");
        REQUIRE(JsonValue(std::vector<int>{}).value() == "[]");

        auto set = std::set<int>({1,2,3,4,5});
        auto jset = JsonValue(set);
        REQUIRE(jset.value() == "[1,2,3,4,5]");
        REQUIRE(JsonValue(std::set<int>{}).value() == "[]");

        auto map = std::map<int,std::string>({{1, "1"}, {2,"2"}});
        auto jmap = JsonValue(map);
        REQUIRE(jmap.value() == "{\"1\":\"1\",\"2\":\"2\"}");
        REQUIRE(JsonValue(std::map<int,int>{}).value() == "{}");

        // test some use cases to make sure output is always correctly formatted
        auto innerStr0 = std::string("somethingSomething");
        auto innerStrJson0 = JsonValue(innerStr0);
        REQUIRE(innerStrJson0 == JsonValue(innerStrJson0));
        auto innerStr1 = std::string("somethingElse");
        auto innerStrJson1 = JsonValue(innerStr1);
        auto innerStr2 = std::string("somethingDifferent");
        auto innerStrJson2 = JsonValue(innerStr2);
        auto innerStr3 = std::string("somethingCompletelyDifferent");
        auto innerStrJson3 = JsonValue(innerStr3);
        auto innerArray0 = std::array<std::string, 2>{innerStr0, innerStr1};
        auto innerJsonArray0 = std::array<JsonValue, 2>{innerStrJson0, innerStrJson1};
        auto innerArray1 = std::array<std::string, 2>{innerStr2, innerStr3};
        auto innerJsonArray1 = std::array<JsonValue, 2>{innerStrJson2, innerStrJson3};
        REQUIRE(JsonValue(innerArray0) == JsonValue(innerJsonArray0));
        REQUIRE(JsonValue(innerArray1) == JsonValue(innerJsonArray1));
        auto outerArray = std::array<std::array<std::string, 2>, 2>{innerArray0, innerArray1};
        auto outerJsonArray = std::array<JsonValue, 2>{JsonValue(innerJsonArray0), JsonValue(innerJsonArray1)};
        REQUIRE(JsonValue(outerArray) == JsonValue(outerJsonArray));
    }

    SECTION("Test Streaming") {
        auto outstream = std::stringstream(std::ios_base::out);
        SECTION("JsonStreamArray") {
            auto jstream = JsonStreamArray(outstream);
            auto array = std::array<int, 2>{4,2};
            auto jsonArray = JsonValue(array);
            jstream << array << array;
            jstream.addValue(array);
            jstream << jsonArray;
            jstream.addValue(jsonArray);
            jstream.close();
            auto string = outstream.str();
            REQUIRE(string == "[[4,2],[4,2],[4,2],[4,2],[4,2]]");
        }
        SECTION("JsonStreamArray with newline") {
            auto jstream = JsonStreamArray(outstream, true);
            auto array = std::array<int, 2>{4,2};
            auto jsonArray = JsonValue(array);
            jstream << array << array;
            jstream.addValue(array);
            jstream << jsonArray;
            jstream.addValue(jsonArray);
            jstream.close();
            auto string = outstream.str();
            REQUIRE(string == "[[4,2],\n[4,2],\n[4,2],\n[4,2],\n[4,2]]");
        }
        SECTION("JsonStreamDict") {
            auto jstream = JsonStreamDict(outstream);
            std::map<int, int> map;
            map[1] = 10;
            map[2] = 20;
            map[3] = 30;
            for (auto&& elem : map) {
                jstream.addValue(std::to_string(elem.first), elem.second);
            }
            jstream.addValue("somethingCompletelyDifferent", true);
            JsonValue innerDict{std::map<int,bool>{{0, false}}};
            jstream.addValue("innerDict", innerDict);
            jstream.close();
            auto string = outstream.str();
            REQUIRE(string == "{\"1\":10,\"2\":20,\"3\":30,\"somethingCompletelyDifferent\":true,\"innerDict\":{\"0\":false}}");
        }
        SECTION("JsonStreamDict with newline") {
            auto jstream = JsonStreamDict(outstream, true);
            std::map<int, int> map;
            map[1] = 10;
            map[2] = 20;
            map[3] = 30;
            for (auto&& elem : map) {
                jstream.addValue(std::to_string(elem.first), elem.second);
            }
            jstream.addValue("somethingCompletelyDifferent", true);
            JsonValue innerDict{std::map<int,bool>{{0, false}}};
            jstream.addValue("innerDict", innerDict);
            jstream.close();
            auto string = outstream.str();
            REQUIRE(string == "{\"1\":10,\n\"2\":20,\n\"3\":30,\n\"somethingCompletelyDifferent\":true,\n\"innerDict\":{\"0\":false}}");
        }
    }
}

TEST_CASE("Recreate Testfiles") {
    using json = nlohmann::json;
    // read testfile without spaces/linebreaks for comparison
    std::ifstream iarray("../testdata/testArray.json");
    std::ifstream iobject("../testdata/testObject.json");
    json array;
    json object;
    iarray >> array;
    iobject >> object;

    // custom structure with custom json creator needed for friends sub-object
    struct Friends {
        Friends(int id, std::string name) {
            auto outstream = std::stringstream(std::ios_base::out);
            auto jstream = JsonStreamDict(outstream, false);
            jstream.addValue("id", id);
            jstream.addValue("name", name);
            jstream.close();
            json = outstream.str();
        }
        std::string json;
    };
    // hardcode json creation
    auto elem0 = [](){
        std::map<std::string, JsonValue> dict;
        dict["_id"] = "5eb94c1a6d56e6ab8ae2fded";
        dict["index"] = 0;
        dict["guid"] = "1d1dd5d4-2f65-464f-97f4-e39ceb6bfc89";
        dict["isActive"] = true;
        dict["balance"] = "$3,568.74";
        dict["picture"] = "http://placehold.it/32x32";
        dict["age"] = 40;
        dict["eyeColor"] = "brown";
        dict["name"] = std::map<std::string, std::string>{
            {"first", "Margie"},
            {"last", "Rodriquez"}};
        dict["company"] = "LUMBREX";
        dict["email"] = "margie.rodriquez@lumbrex.info";
        dict["phone"] = "+1 (880) 448-2882";
        dict["address"] = "224 Court Square, Torboy, Arizona, 766";
        dict["about"] = "Reprehenderit ex est nulla commodo irure Lorem dolor tempor anim ullamco. Officia ullamco ad exercitation sit aliquip nisi veniam ipsum tempor. Veniam sunt excepteur fugiat veniam quis id et proident aute culpa dolor ad enim Lorem. Quis deserunt elit commodo dolor eiusmod magna esse aliquip.";
        dict["registered"] = "Sunday, May 19, 2019 8:36 PM";
        dict["latitude"] = "-9.19402";
        dict["longitude"] = "-84.688783";
        dict["tags"] = std::vector<std::string>{
                "veniam",
                "dolore",
                "ipsum",
                "aliquip",
                "eu"};
        dict["range"] = std::array<int, 10>{0,1,2,3,4,5,6,7,8,9};
        dict["friends"] = std::vector<JsonValue>{
                JsonValue(Friends(0, "Baird Sanford").json, JsonValue::stringIsValidJson),
                JsonValue(Friends(1, "Hyde Henry").json, JsonValue::stringIsValidJson),
                JsonValue(Friends(2, "Rowena Ramirez").json, JsonValue::stringIsValidJson)};
        dict["greeting"] = "Hello, Margie! You have 5 unread messages.";
        dict["favoriteFruit"] = "banana";
        return JsonValue(dict);
    };
    auto elem1 = [](){
        std::map<std::string, JsonValue> dict;
        dict["_id"] = "5eb94c1a42ef5ac6e6e92f02";
        dict["index"] = 1;
        dict["guid"] = "c465b233-52f9-4c84-985e-24e7c595a190";
        dict["isActive"] = true;
        dict["balance"] = "$3,817.99";
        dict["picture"] = "http://placehold.it/32x32";
        dict["age"] = 20;
        dict["eyeColor"] = "blue";
        dict["name"] = std::map<std::string, std::string>{
            {"first", "Roth"},
            {"last", "Nixon"}};
        dict["company"] = "EXOSIS";
        dict["email"] = "roth.nixon@exosis.co.uk";
        dict["phone"] = "+1 (840) 434-2926";
        dict["address"] = "910 Nassau Street, Springhill, Connecticut, 3973";
        dict["about"] = "Reprehenderit aliquip dolor eu eiusmod commodo nulla laboris velit. Enim duis labore nostrud sit do magna sint duis proident non non aliquip. Ad culpa laborum et et aute ipsum nisi sint dolor excepteur elit commodo. Laborum pariatur anim Lorem sint amet duis dolore nisi nostrud enim. Mollit do occaecat cupidatat irure excepteur excepteur nostrud. Aliqua elit in mollit aliquip voluptate sunt sint laboris voluptate. Commodo culpa veniam qui consectetur esse consectetur eiusmod.";
        dict["registered"] = "Wednesday, December 2, 2015 7:51 AM";
        dict["latitude"] = "-86.482186";
        dict["longitude"] = "70.600031";
        dict["tags"] = std::vector<std::string>{
                "minim",
                "nostrud",
                "nulla",
                "quis",
                "consectetur"};
        dict["range"] = std::array<int, 10>{0,1,2,3,4,5,6,7,8,9};
        dict["friends"] = std::vector<JsonValue>{
                JsonValue(Friends(0, "Compton Young").json, JsonValue::stringIsValidJson),
                JsonValue(Friends(1, "Martinez Lyons").json, JsonValue::stringIsValidJson),
                JsonValue(Friends(2, "Aimee Rivers").json, JsonValue::stringIsValidJson)};
        dict["greeting"] = "Hello, Roth! You have 5 unread messages.";
        dict["favoriteFruit"] = "apple";
        return JsonValue(dict);
    };
    auto elem2 = [](){
        std::map<std::string, JsonValue> dict;
        dict["_id"] = "5eb94c1a68bf898b45529020";
        dict["index"] = 2;
        dict["guid"] = "0dc8c57f-0424-46c0-845b-350cb1f9f2e8";
        dict["isActive"] = true;
        dict["balance"] = "$3,738.11";
        dict["picture"] = "http://placehold.it/32x32";
        dict["age"] = 34;
        dict["eyeColor"] = "blue";
        dict["name"] = std::map<std::string, std::string>{
            {"first", "Gallegos"},
            {"last", "Dawson"}};
        dict["company"] = "CENTICE";
        dict["email"] = "gallegos.dawson@centice.tv";
        dict["phone"] = "+1 (988) 544-3909";
        dict["address"] = "292 Vernon Avenue, Helen, South Dakota, 9816";
        dict["about"] = "Officia duis ipsum laboris enim voluptate. Culpa do aliqua aliqua deserunt dolore labore reprehenderit officia labore cillum anim nulla. Ipsum ea ullamco adipisicing magna aute sunt. Occaecat sit nulla proident ipsum et adipisicing in do velit fugiat consectetur fugiat ex sint. Do pariatur mollit in esse reprehenderit mollit occaecat do anim qui voluptate commodo. In ex sint officia commodo do fugiat minim dolor cupidatat aliqua.";
        dict["registered"] = "Saturday, January 4, 2020 8:10 PM";
        dict["latitude"] = "48.071905";
        dict["longitude"] = "-71.898632";
        dict["tags"] = std::vector<std::string>{
                "eu",
                "enim",
                "excepteur",
                "qui",
                "adipisicing"};
        dict["range"] = std::array<int, 10>{0,1,2,3,4,5,6,7,8,9};
        dict["friends"] = std::vector<JsonValue>{
                JsonValue(Friends(0, "Stefanie Pena").json, JsonValue::stringIsValidJson),
                JsonValue(Friends(1, "Susanne Strong").json, JsonValue::stringIsValidJson),
                JsonValue(Friends(2, "Cardenas Savage").json, JsonValue::stringIsValidJson)};
        dict["greeting"] = "Hello, Gallegos! You have 6 unread messages.";
        dict["favoriteFruit"] = "banana";
        return JsonValue(dict);
    };

    // assemble json array
    auto outstream = std::stringstream(std::ios_base::out);
    auto arrayStream = JsonStreamArray(outstream, false);
    arrayStream.addValue(elem0());
    arrayStream.addValue(elem1());
    arrayStream.addValue(elem2());
    arrayStream.close();

    REQUIRE(outstream.str() == array.dump());

    // assemble json object
    outstream = std::stringstream(std::ios_base::out);
    auto dictStream = JsonStreamDict(outstream, false);
    dictStream.addValue("0", elem0());
    dictStream.addValue("1", elem1());
    dictStream.addValue("2", elem2());
    dictStream.close();

    REQUIRE(outstream.str() == object.dump());
}

TEST_CASE("Test Readme Examples") {
    struct Person {
        Person(std::string fname, std::string lname, int age)
            : first_name{fname}, last_name{lname}, age{age} {}
        std::string first_name;
        std::string last_name;
        int age;
        auto toJsonValue() const {
            auto outstream = std::stringstream(std::ios_base::out);
            auto jstream = JsonStreamDict(outstream);
            jstream.addValue("first_name", first_name);
            jstream.addValue("last_name", last_name);
            jstream.addValue("age", age);
            jstream.close();
            return JsonValue(outstream.str(), JsonValue::stringIsValidJson);
        }
    };
    auto jd = Person("Jon", "Doe", 42);

    auto outstream = std::stringstream(std::ios_base::out);
    auto arrayStream = JsonStreamArray(outstream);
    arrayStream << "something" << 123 << std::map<int, int>{{1,10},{2,20}};
    arrayStream.addValue(std::set<int>{1,2,3});
    arrayStream.addValue(JsonValue("[\"valid\", \"JSON\"]", JsonValue::stringIsValidJson));
    arrayStream << jd.toJsonValue();
    arrayStream.close();
    REQUIRE(outstream.str() == "[\"something\",123,{\"1\":10,\"2\":20},[1,2,3],[\"valid\", \"JSON\"],{\"first_name\":\"Jon\",\"last_name\":\"Doe\",\"age\":42}]");

    outstream = std::stringstream(std::ios_base::out);
    auto dictStream = JsonStreamDict(outstream);
    dictStream.addValue("0", 0);
    dictStream.addValue("1", std::map<int,int>{{2,3},{4,5}});
    dictStream.close();
    REQUIRE(outstream.str() == "{\"0\":0,\"1\":{\"2\":3,\"4\":5}}");
}
