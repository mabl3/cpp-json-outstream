# C++ JSON Outstream

A C++11 library that allows streaming data as JSON arrays or objects into a 
file.

Have a look at https://www.json.org/ to learn about the JSON format.

## Installation

Copy the two files in `include/mabl3` into your project and 
`#include "mabl3/JsonStream.h"`

## Usage

#### Initialization

First initialize a stream to write stuff into, this can be either be a JSON
array or a JSON object (i.e. a number of key-value pairs)

```C++
auto os = std::ofstream("output.json"); // works with all std::basic_ostream<char>

// to create a JSON array
auto arrayStream = JsonStreamArray(os);

// to create a more readable array with newline after each element
auto arrayStreamReadable = JsonStreamArray(os, true);

// to create a JSON object
auto dictStream = JsonDictStream(os);

// to create a more readable object with newline after each element
auto dictStreamReadable = JsonDictStream(os, true);
```

#### Writing

Then you can start appending elements to the stream. For arrays, you can use the
stream `operator<<` or the `addValue` method

```C++
arrayStream << "something" << 123 << std::map<int, int>{{1,10},{2,20}};
arrayStream.addValue(std::set<int>{1,2,3});
arrayStream.addValue(
  JsonValue("[\"valid\", \"JSON\"]", JsonValue::stringIsValidJson)
);
```

Note that in the last example, you need to explicitly construct the `JsonValue` 
and pass the flag `JsonValue::stringIsValidJson`. This way, the valid JSON array
`["valid", "JSON"]` is retained. Without that flag, each string or string 
literal is modified to be a valid JSON string, i.e. appending leading and 
trailing `"` and escaping any internal `"`.

For `JsonDictStream`, there is no stream `operator<<`, so you need to call the
`addElement` method with two arguments, the first a `std::string` (or literal)
with the key and the second any valid element as the value

```C++
dictStream.addValue("0", 0);
dictStream.addValue("1", std::map<int,int>{{2,3},{4,5}});
```

#### Finishing

When you are done streaming your data, don't forget to close the JsonStreams

```C++
arrayStream.close();
dictStream.close();
```

This will put a closing `]` or `}` to the array or object. If the `JsonStream`
objects are destroyed properly (e.g. going out of scope) without prior calling 
of `close()`, this is done by the destructor if you forgot to do this manually.

The JSON strings generated in above code snippets are

`["something",123,{"1":10,"2":20},[1,2,3],["valid", "JSON"]]`

and

`{"0":0,"1":{"2":3,"4":5}}`

## JsonValue

C++ JSON Outstream can handle a number of types. As of POD, you can pass
anything for which `std::to_string` is defined or is itself a `std::string` or a 
string literal or a `bool`.

You can also pass all STL (or STL-like) containers. The library distinguishes
between "map-containers" and "sequential-containers". Nested containers also 
work.

### Container Details

Containers are distinguished in the following manner: Say you pass a `value` of
type `T`. If `T` is a `std::string` or literal or `bool`, this gets handled
directly. Otherwise, the library tests if `T.begin()` and `T.end()` are defined.
If so, `T` is treated as a container. This is true for all `C++17` containers
listed [here](https://en.cppreference.com/w/cpp/container).

If `T` is not a container, `std::to_string` is called on `value`. If that is not
possible, compilation will fail.

If `T` is a container, the library tests if `T::key_type` and 
`T::value_type::first_type` are defined. This is true for `std::map` and the 
likes. These are transformed into JSON objects. The `key`s always get 
transformed into JSON strings. All other containers (`std::vector`, `std::set` 
etc.) are treated as sequential containers and therefore transformed into JSON 
arrays.

#### Non-STL Containers

If your container implementations resemble STL containers close enough, this
should still work. I successfully used this library with 
[tsl::hopscoth_map](https://github.com/Tessil/hopscotch-map), for example.

### Custom Classes

If you want to turn something into JSON that is not described above, it's easy
to write your own serializer. 

Say you have an object of `struct Person` that you want to turn into a JSON 
object with mixed value types. This can be achieved by something like this:

```C++
struct Person {
    Person(std::string fname, std::string lname, int age)
        : first_name{fname}, last_name{lname}, age{age} {}
        
    std::string first_name;
    std::string last_name;
    int age;
    
    auto toJsonValue() const {
        auto outstream = std::stringstream(std::ios_base::out);
        auto jstream = JsonStreamDict(outstream);
        jstream.addValue("first_name", first_name);
        jstream.addValue("last_name", last_name);
        jstream.addValue("age", age);
        jstream.close();
        return JsonValue(outstream.str(), JsonValue::stringIsValidJson);
    }
};
```

`Person::toJasonValue()` returns a `JsonValue` that can directly be fed into
the `JsonStream`

```C++
auto jd = Person("Jon", "Doe", 42);
arrayStream << jd.toJasonValue();
```

resulting in

`{"first_name":"Jon","last_name":"Doe","age":42}`

## Testing

You can run the unit tests yourself. For this, clone this repo recursively to
get the dependencies

`$ git clone --recurse-submodules https://gitlab.com/mabl3/cpp-json-outstream`

If you already cloned this repo but forgot the submodules, run

`$ git submodule update --init --recursive`

Run the following commands to compile and run the tests

```
cd test
mkdir build && cd build
cmake .. && make
./testJsonStream
```

#### Acknowledgements

Unit test are created with [Catch2](https://github.com/catchorg/Catch2)

In some tests, [nlohmann::json](https://github.com/nlohmann/json) is used for
comparison

JSON testfiles have been generated with the 
[JSON Generator](https://next.json-generator.com)